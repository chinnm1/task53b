public class App {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author("Mai", "mai@gmail.com", 'f');
        Author author2 = new Author("Nam", "nam@gmail.com", 'm');
        System.out.println(author1.toString());
        System.out.println(author2.toString());
        Book book1 = new Book("Math", author1, 50000);
        Book book2 = new Book("Ënglish", author2, 100000, 3);
        System.err.println(book1.toString());
        System.out.println(book2.toString());
    }
}
