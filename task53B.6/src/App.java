public class App {
    public static void main(String[] args) throws Exception {
        Animal animal1 = new Animal("Vit");
        Animal animal2 = new Animal("Trau");
        System.out.println(animal1.toString());
        System.out.println(animal2.toString());

        Mammal mammal1 = new Mammal("Bo");
        Mammal mammal2 = new Mammal("Cuu");
        System.out.println(mammal1.toString());
        System.out.println(mammal2.toString());

        Cat cat1 = new Cat("Tam The");
        Cat cat2 = new Cat("Meo den");
        System.out.println(cat1.toString());
        System.out.println(cat2.toString());
        cat1.greets();
        cat2.greets();

        Dog dog1 = new Dog("Vang");
        Dog dog2 = new Dog("Muc");
        System.out.println(dog1.toString());
        System.out.println(dog2.toString());
        dog1.greets();
        dog2.greets();
        dog1.greets(dog2);

    }
}
