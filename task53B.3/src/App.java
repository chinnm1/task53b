public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(01, "Mai", 10);
        Customer customer2 = new Customer(02, "Chi", 15);
        System.out.println(customer2.toString());
        System.out.println(customer1.toString());

        Invoice invoice1 = new Invoice(01, customer1, 3);
        Invoice invoice2 = new Invoice(02, customer2, 5);
        System.out.println(invoice1.toString());
        System.out.println(invoice2.toString());
    }
}
