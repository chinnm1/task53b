public class App {
    public static void main(String[] args) throws Exception {
        Shape shape1 = new Shape();
        Shape shape2 = new Shape("green", false);
        System.out.println(shape1.toString());
        System.out.println(shape2.toString());

        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(2.5, 1.5);
        Rectangle rectangle3 = new Rectangle("green", true, 2, 1.5);

        System.out.println(rectangle1.toString());
        System.out.println(rectangle2.toString());
        System.out.println(rectangle3.toString());

        Square square1 = new Square();
        Square square2 = new Square(1.5);
        Square square3 = new Square("green", true, 2.0);

        System.out.println(square1.toString());
        System.out.println(square2.toString());
        System.out.println(square3.toString());

        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle(3.0, "green", true);

        System.out.println(circle1.toString());
        System.out.println(circle2.toString());
        System.out.println(circle3.toString());

        System.out.println("Dien tich hinh tron" + circle2.getArea());
        System.out.println("CHu vi hinhf tron" + circle2.getParimeter());

        System.out.println("Dien tich hinh chu nhat" + rectangle2.getArea());
        System.out.println("Chu vi hinh chu nhat" + rectangle2.getPerimeter());

        square2.setWidth(2.0);
        square2.setLength(2.0);

        System.out.println("Dien tich hinh vuong " + square2.getArea());
        System.out.println("Chu vi hinh vuong" + square2.getPerimeter());
    }
}
