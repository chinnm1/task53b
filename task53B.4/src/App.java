public class App {
    public static void main(String[] args) throws Exception {
        Person person1 = new Person("Mai", "Viet Nam");
        Person person2 = new Person("Chi", "Korea");

        System.out.println(person1.toString());
        System.out.println(person2.toString());

        Student student1 = new Student("Lan", "America", "Science", 4, 100000);
        Student student2 = new Student("Nam", "CHina", "Math", 5, 200000);

        System.out.println(student1.toString());
        System.out.println(student2.toString());

        Staff staff1 = new Staff("Hoa", "Germany", "ABC", 23423423);
        Staff staff2 = new Staff("Dao", "Japan", "XYZ", 98888);

        System.out.println(staff1.toString());
        System.out.println(staff2.toString());
    }
}
