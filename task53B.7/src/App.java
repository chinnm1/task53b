import javax.xml.crypto.Data;

import java.util.Date;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer("Minh");
        Customer customer2 = new Customer("Trang");

        Visit visit1 = new Visit(customer1, new Date(), 1000000, 20000);
        System.out.println(visit1.toString());

        System.out.println(visit1.getTotalExpense());
    }
}
